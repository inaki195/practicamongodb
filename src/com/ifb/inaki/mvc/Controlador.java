package com.ifb.inaki.mvc;

import com.ifb.inaki.base.Movil;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, KeyListener {
    Modelo modelo;
    Vista vista;

    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        addlistener(this);
        addListSelectionListeners(this);
        addKeyListeners(this);

    }

    private void addKeyListeners(KeyListener listener) {
        vista.txtbusqueda.addKeyListener(listener);
    }


    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    /**
     * add listener a los botones para poder interactuar
     * @param listener se add  al boton para que halla comunicacion
     */
    private void addlistener(ActionListener listener) {
        vista.btnadd.addActionListener(listener);
        vista.btnmodificar.addActionListener(listener);
        vista.btndelete.addActionListener(listener);
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    String comando=e.getActionCommand();
    switch (comando){
        case "add":{
            Movil movil = new Movil();
            try {
                movil.setMarca(vista.txtmarca.getText());
                movil.setModelo(vista.txtModelo.getText());
                movil.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                movil.setFechaLanzamienta(vista.fechalanzamiento.getDate());
            }catch (NumberFormatException e1){
                JOptionPane.showMessageDialog(null,"selecciona un objeto en la lista","no puedes eliminar",JOptionPane.WARNING_MESSAGE);
            }
            modelo.addMovil(movil);
            ArrayList<Movil>listamoviles= (ArrayList<Movil>) modelo.getMovil();
            actualizarLista(listamoviles);
        }
        break;
        case "delete":{
            try {
                Movil movil = (Movil) vista.list1.getSelectedValue();
                modelo.borrarMovil(movil);
                ArrayList<Movil> listamoviles = (ArrayList<Movil>) modelo.getMovil();
                actualizarLista(listamoviles);
            }catch (NullPointerException e1){
                JOptionPane.showMessageDialog(null,"selecciona un objeto en la lista","no puedes eliminar",JOptionPane.WARNING_MESSAGE);
            }
        }
        break;
        case "alter":{
            Movil movil= (Movil) vista.list1.getSelectedValue();
            modificarMovilFromCampos(movil);
            modelo.modificarMovil(movil);
            ArrayList<Movil>listamoviles= (ArrayList<Movil>) modelo.getMovil();
            actualizarLista(listamoviles);
        }
        break;
        case "conectar":{
            modelo.conectar();
            ArrayList<Movil>listamoviles= (ArrayList<Movil>) modelo.getMovil();
            actualizarLista(listamoviles);
        }
        break;

        case "salir":{
            modelo.desconectar();
        }
        break;
    }
    }

    private void modificarMovilFromCampos(Movil movil) {
        movil.setModelo(vista.txtModelo.getText());
        movil.setMarca(vista.txtmarca.getText());
        movil.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
        movil.setFechaLanzamienta(vista.fechalanzamiento.getDate());
    }

    private void actualizarLista(ArrayList<Movil> listamoviles) {
        vista.dlmlistamoviles.clear();
        for (Movil movil:listamoviles){
            vista.dlmlistamoviles.addElement(movil);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            Movil movil= (Movil) vista.list1.getSelectedValue();
            vista.txtmarca.setText(movil.getMarca());
            vista.txtModelo.setText(movil.getModelo());
            vista.txtPrecio.setText(String.valueOf(movil.getPrecio()));
            vista.fechalanzamiento.setDate(movil.getFechaLanzamienta());
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtbusqueda){
            listarMovil(modelo.getmovil(vista.txtbusqueda.getText()));

        }

    }

    private void listarMovil(List<Movil> moviles) {
        vista.dlmlistamoviles.clear();
        for (Movil movil:moviles){
            vista.dlmlistamoviles.addElement(movil);
        }

    }
}
