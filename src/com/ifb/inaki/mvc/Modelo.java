package com.ifb.inaki.mvc;

import com.ifb.inaki.base.Movil;
import com.ifb.inaki.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {
    private  final String COLECCION_MOVILES="moviles";
    private  final String DATABASE="phonehouse";
    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection collectionCoches;

    public void conectar() {
        client=new MongoClient();
        baseDatos=client.getDatabase(DATABASE);
        collectionCoches=baseDatos.getCollection(COLECCION_MOVILES);
    }
    public void desconectar(){
        client.close();
    }


    public void addMovil(Movil movil) {
        collectionCoches.insertOne(movilToDocument(movil));
    }

    public List<Movil> getMovil(){
        ArrayList<Movil> lista = new ArrayList<>();
        Iterator<Document> it=collectionCoches.find().iterator();
        while (it.hasNext()){
            lista.add(documentToMovil(it.next()));
        }
        return lista;
    }

    /**
     * Listo los coches atendiendo a un 2 criterios basados en expresiones regulares.
     * @param text cadena que quiero que aparezca en las matriculas o marcas
     * @return
     */
    public List<Movil> getmovil(String text) {
        ArrayList<Movil> lista = new ArrayList<>();
        Document query=new Document();
        List<Document>listaCriteriios=new ArrayList<>();
        listaCriteriios.add(new Document("marca",new Document("$regex","/*"+text+"/*")));
        listaCriteriios.add(new Document("modelo",new Document("$regex","/*"+text+"/*")));
        query.append("$or",listaCriteriios);
        Iterator<Document>it=collectionCoches.find(query).iterator();
        while (it.hasNext()){
            lista.add(documentToMovil(it.next()));
        }
        return lista;
    }

    public Document movilToDocument(Movil movil){
        Document documento = new Document();
        documento.append("marca",movil.getMarca());
        documento.append("modelo",movil.getModelo());
        documento.append("precio",movil.getPrecio());
        documento.append("fechaLanzamiento", Util.formatearFecha(movil.getFechaLanzamienta()));
        return documento;
    }

    public Movil documentToMovil(Document document){
        Movil movil = new Movil();
        movil.setId(document.getObjectId("_id"));
        movil.setMarca(document.getString("marca"));
        movil.setModelo(document.getString("modelo"));
        movil.setPrecio(Float.parseFloat(String.valueOf(document.getDouble("precio"))));
        movil.setFechaLanzamienta(Util.parsearFecha(document.getString("fechaLanzamiento")));
        return movil;
    }

    public void modificarMovil(Movil movil) {
        collectionCoches.replaceOne(new Document("_id",movil.getId()),movilToDocument(movil));

    }

    public void borrarMovil(Movil movil) {
        collectionCoches.deleteOne(movilToDocument(movil));
    }

}
