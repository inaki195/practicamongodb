package com.ifb.inaki.mvc;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vista {
    private JPanel panel1;
     JTextField txtmarca;
     JTextField txtModelo;
     JTextField txtPrecio;
     JList list1;
     JTextField txtram;
     JButton btnadd;
     JButton btnmodificar;
     JButton btndelete;
    JButton btnBuscar;
     DatePicker fechalanzamiento;
     JTextField txtbusqueda;
    JFrame frame;
     JMenuItem conexionItem;
     JMenuItem salirItem;
     DefaultListModel dlmlistamoviles;

    public Vista() {
        frame = new JFrame("moviles");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(450,600);
        frame.setLocationRelativeTo(null);
        crearmenu();
        inicializarlist();
    }

    private void inicializarlist() {
        dlmlistamoviles=new DefaultListModel();
        list1.setModel(dlmlistamoviles);
    }

    private void crearmenu() {
        JMenuBar barra=new JMenuBar();
        JMenu menu=new JMenu("archivo");
        conexionItem = new JMenuItem("conectar");
        conexionItem.setActionCommand("conectar");
        salirItem = new JMenuItem("salir");
        salirItem.setActionCommand("salir");
        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

}
