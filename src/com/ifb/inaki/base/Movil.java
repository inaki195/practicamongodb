package com.ifb.inaki.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Movil {
    private ObjectId id;
    private String marca;
    private String modelo;
    private float precio;

    private LocalDate fechaLanzamienta;

    @Override
    public String toString() {
        return marca  + " " + modelo  + "  " + precio + "€ ";
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Movil() {
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }


    public LocalDate getFechaLanzamienta() {
        return fechaLanzamienta;
    }

    public void setFechaLanzamienta(LocalDate fechaLanzamienta) {
        this.fechaLanzamienta = fechaLanzamienta;
    }
}
