package com.ifb.inaki.principal;

import com.ifb.inaki.base.Movil;
import com.ifb.inaki.mvc.Controlador;
import com.ifb.inaki.mvc.Modelo;
import com.ifb.inaki.mvc.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista=new Vista();
        Modelo modelo=new Modelo();
        Controlador controlador=new Controlador(modelo,vista);

    }
}
